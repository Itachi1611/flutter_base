import 'package:flutter_base/helpers/shared_prefs_helper.dart';
import 'package:flutter_base/models/entities/token_entity.dart';
import 'package:flutter_base/network/api_client.dart';

abstract class AuthRepository {
  Future<TokenEntity> getToken();

  Future<bool> saveToken(TokenEntity token);

  Future<bool> removeToken();

  Future<void> signOut();
}

class AuthRepositoryImpl extends AuthRepository {
  ApiClient _apiClient;

  AuthRepositoryImpl(ApiClient client) {
    _apiClient = client;
  }

  @override
  Future<TokenEntity> getToken() async {
    return await SharedPrefsHelper.getToken();
  }

  @override
  Future<bool> removeToken() async {
    return await SharedPrefsHelper.removeToken();
  }

  @override
  Future<bool> saveToken(TokenEntity token) async {
    return await SharedPrefsHelper.saveToken(token);
  }

  @override
  Future<void> signOut() async {
    return await Future.wait([SharedPrefsHelper.removeToken(),]);
  }
}
