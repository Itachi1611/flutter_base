import 'dart:convert';
import 'package:flutter_base/models/entities/token_entity.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefsHelper {
  ///Define key
  static final String _tokenPrefs = 'token';

  ///Token
  static Future<bool> saveToken(TokenEntity token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(_tokenPrefs, json.encode(token.toJson()));
  }

  static Future<TokenEntity> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString(_tokenPrefs);
    if (token == null) {
      return null;
    } else {
      try {
        return TokenEntity.fromJson(json.decode(token));
      } catch (e) {
        print(e);
        return null;
      }
    }
  }

  static Future<bool> removeToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.remove(_tokenPrefs);
  }
}
