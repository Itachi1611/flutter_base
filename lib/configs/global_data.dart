import 'package:flutter_base/models/entities/token_entity.dart';

class GlobalData {
  GlobalData._privateConstructor();

  static final GlobalData instance = GlobalData._privateConstructor();

  TokenEntity token;

  ///Clear data when logout
  void clearData() {
    if (token != null) {
      token = null;
    }
  }
}
