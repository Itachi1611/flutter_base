class AppConfig {
  static const String appId = 'fox';
  static const String appName = 'Flutter Base';
  static const String version = '1.0.0';

  ///Network
  static const baseUrl = "";

  ///Paging
  static const pageSize = 40;
  static const pageSizeMax = 1000;
}

class FirebaseConfig {
  //Todo
}

class DatabaseConfig {
  //Todo
  static const int version = 1;
}

class MovieAPIConfig {
  static const String APIKey = '26763d7bf2e94098192e629eb975dab0';
}
