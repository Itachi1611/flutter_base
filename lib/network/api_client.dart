import 'package:dio/dio.dart' hide Headers;
import 'package:flutter_base/configs/app_config.dart';
import 'package:retrofit/retrofit.dart';

part 'api_client.g.dart';

@RestApi(baseUrl: AppConfig.baseUrl)
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  /// GET Master Data
  @GET("/api/auth/dictionaries")
  Future<dynamic> getDictionaries(@Query("language_id") int languageId);
}
