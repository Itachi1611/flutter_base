import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_base/blocs/app_cubit.dart';
import 'package:flutter_base/models/enums/flush_bar_type.dart';
import 'package:flutter_base/utils/fox_flush_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'commons/app_themes.dart';
import 'generated/l10n.dart';
import 'network/api_client.dart';
import 'network/api_util.dart';
import 'repositories/auth_repository.dart';
import 'router/application.dart';
import 'router/routers.dart';
import 'utils/fox_connection_status.dart';

final navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  //Init HiveBox and register adapter for Hive
  await Hive.initFlutter();
  //Hive.registerAdapter(adapter);
  //await Hive.openBox<T>(key);

  //Set the orientation
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    //DeviceOrientation.portraitDown,
    //DeviceOrientation.landscapeLeft,
    //DeviceOrientation.landscapeRight
  ]).then((_) {
    runApp(new MyApp());
  });
}

class MyApp extends StatefulWidget {
  MyApp() {
    final router = new FluroRouter();
    Routes.configureRoutes(router);
    Application.router = router;
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Connectivity _connectivity = Connectivity();

  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  StreamSubscription _connectionChangeStream;

  AppCubit _appCubit;

  ApiClient _apiClient;

  @override
  void initState() {
    _apiClient = ApiUtil.getApiClient();

    _appCubit = AppCubit();

    super.initState();

    initConnectivity();

    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    _connectionChangeStream = _appCubit.connectionChangeController.stream.listen((event) {
      if(event == false) {
        FoxFlushBar.showFlushBar(context, type: FlushBarType.ERROR, message: S.current.connection_error);
      }
    });


  }

  void connectionChanged(dynamic hasConnection) => _appCubit.onChangeConnectionStatus(hasConnection);

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
     _appCubit.onChangeConnectionStatus(result);
  }

  @override
  Widget build(BuildContext context) {
    AuthRepository authRepository = AuthRepositoryImpl(_apiClient);

    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<AuthRepository>(create: (context) {
          return authRepository;
        }),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AppCubit>(create: (context) {
            return AppCubit();
          }),
        ],
        child: BlocBuilder<AppCubit, AppState>(
          buildWhen: (prev, current) => false,
          builder: (context, state) {
            return MaterialApp(
              navigatorKey: navigatorKey,
              debugShowCheckedModeBanner: false,
              theme: AppThemes.theme,
              onGenerateRoute: Application.router.generator,
              initialRoute: Routes.root,
              localizationsDelegates: [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                S.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              builder: (context, child) {
                return MediaQuery(
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  child: child,
                );
              },
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    Hive.close();
    _connectivitySubscription.cancel();
    _connectionChangeStream.cancel();
    super.dispose();
  }
}
