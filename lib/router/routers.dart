import 'package:fluro/fluro.dart';
import './router_handler.dart';

class Routes {
  static String root = "/";

  static void configureRoutes(FluroRouter router) {
    router.notFoundHandler = notHandler;
    router.define(root, handler: splashHandler);
  }
}
