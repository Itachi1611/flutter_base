import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/ui/pages/splash/splash_page.dart';

Handler notHandler = new Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) => Scaffold(
    body: Center(
      child: Text('$params'),
    ),
  ),
);

Handler splashHandler = new Handler(
  handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    return SplashPage();
  },
);