import 'package:equatable/equatable.dart';
import 'package:flutter_base/models/responses/meta_response.dart';

class ObjectResponse<T> extends Equatable {
  final T data;
  final MetaResponse meta;

  ObjectResponse({this.data, this.meta});

  ObjectResponse<T> copyWith({
    T data,
    MetaResponse meta,
  }) =>
      ObjectResponse<T>(
        data: data ?? this.data,
        meta: meta ?? this.meta,
      );

  factory ObjectResponse.fromJson(Map<String, dynamic> json, T Function(Object json) fromJsonT) {
    ObjectResponse<T> resultGeneric = ObjectResponse<T>();
    if (json['data'] != null) {
      if (json['data'] is String) {
        resultGeneric = resultGeneric.copyWith(
          data: null,
        );
      } else if (json['data'] is! List) {
        resultGeneric = resultGeneric.copyWith(
          data: fromJsonT(json['data']),
        );
      }
    }
    if (json['meta'] != null) {
      if (json['meta'] is! List){
        resultGeneric = resultGeneric.copyWith(
          meta: MetaResponse.fromJson(json['meta']),
        );
      }
    }
    return resultGeneric;
  }

  @override
  List<Object> get props => [
    data,
    meta,
  ];
}
