import 'package:equatable/equatable.dart';

import 'meta_response.dart';

class ArrayResponse<T> extends Equatable { // ignore: must_be_immutable
  final int page;
  final int pageSize;
  final int totalPages;
  List<T> results;
  MetaResponse metaData;

  ArrayResponse({
    this.page,
    this.pageSize,
    this.totalPages,
    this.results,
    this.metaData,
  });

  ArrayResponse copyWith({
    int page,
    int pageSize,
    int totalPages,
    List<T> results,
    MetaResponse metaData,
  }) {
    return new ArrayResponse(
      page: page ?? this.page,
      pageSize: pageSize ?? this.pageSize,
      totalPages: totalPages ?? this.totalPages,
      results: results ?? this.results,
      metaData: metaData ?? this.metaData,
    );
  }

  factory ArrayResponse.fromJson(Map<String, dynamic> json, T Function(Object json) fromJsonT) {
    ArrayResponse<T> resultGeneric = ArrayResponse<T>(
      page: json['page'] as int ?? 1,
      pageSize: json['page_size'] as int ?? 1,
      totalPages: json['total_pages'] as int ?? 1,
    );
    if (json['meta'] != null) {
      if (json['meta'] is! List) {
        final metaData = MetaResponse.fromJson(json['meta']);
        resultGeneric.metaData = metaData;
      }
    }
    if (json['data'] != null) {
      if (json['data'] is List) {
        final data = (json['data'] as List)?.map(fromJsonT)?.toList();
        resultGeneric.results = data;
      }
    }
    return resultGeneric;
  }

  @override
  List<Object> get props => [
    page,
    pageSize,
    totalPages,
    results,
  ];
}
