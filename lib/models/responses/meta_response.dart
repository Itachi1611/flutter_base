import 'package:json_annotation/json_annotation.dart';

part 'meta_response.g.dart';

@JsonSerializable()
class MetaResponse {
  @JsonKey(name: 'limit')
  int limit;
  @JsonKey(name : 'offset')
  int offset;
  @JsonKey(name : 'total')
  int total;

  MetaResponse(this.limit, this.offset, this.total);

  factory MetaResponse.fromJson(Map<String, dynamic> json) => _$MetaResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MetaResponseToJson(this);
}