class AppDimens {
  AppDimens._(); // this basically makes it so you can instantiate this class

  //TextFontSize
  static const double fontSmallest = 10.0;
  static const double fontSmaller = 11.0;
  static const double fontSmall = 12.0;
  static const double fontNormal = 14.0;
  static const double fontLarge = 16.0;
  static const double fontLarger = 17.0;
  static const double fontLargest = 18.0;

  //Common
  static const double appBarHeight = 48.0;
}
