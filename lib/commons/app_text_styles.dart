import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_base/commons/app_dimens.dart';

import 'app_colors.dart';

class AppTextStyle {

  static final regularStyle = TextStyle(fontFamily: "Roboto");
  static final italicStyle = TextStyle(fontFamily: "Roboto", fontStyle: FontStyle.italic);
  static final boldStyle = TextStyle(fontFamily: "Roboto", fontWeight: FontWeight.bold);

  ///Black
  static final blackRegular = regularStyle.copyWith(color: Colors.black);
  static final blackItalic = italicStyle.copyWith(color: Colors.black);
  static final blackBold = boldStyle.copyWith(color: Colors.black);

  //s12
  static final blackS12Regular = blackRegular.copyWith(fontSize: 12);
  static final blackS12Italic = blackItalic.copyWith(fontSize: 12);
  static final blackS12Bold = blackBold.copyWith(fontSize: 12);

  //s13
  static final blackS13Regular = blackRegular.copyWith(fontSize: 13);
  static final blackS13Italic = blackItalic.copyWith(fontSize: 13);
  static final blackS13Bold = blackBold.copyWith(fontSize: 13);

  //s14
  static final blackS14Regular = blackRegular.copyWith(fontSize: 14);
  static final blackS14Italic = blackItalic.copyWith(fontSize: 14);
  static final blackS14Bold = blackBold.copyWith(fontSize: 14);

  //s16
  static final blackS15Regular = blackBold.copyWith(fontSize: 15);
  static final blackS15Italic = blackItalic.copyWith(fontSize: 15);
  static final blackS15Bold = blackBold.copyWith(fontSize: 15);

  //s16
  static final blackS16Regular = blackRegular.copyWith(fontSize: 16);
  static final blackS16Italic = blackItalic.copyWith(fontSize: 16);
  static final blackS16Bold = blackBold.copyWith(fontSize: 16);

  //s17
  static final blackS17Regular = blackRegular.copyWith(fontSize: 17);
  static final blackS17Italic = blackItalic.copyWith(fontSize: 17);
  static final blackS17Bold = blackBold.copyWith(fontSize: 17);

  //s18
  static final blackS18Regular = blackRegular.copyWith(fontSize: 18);
  static final blackS18Italic = blackItalic.copyWith(fontSize: 18);
  static final blackS18Bold = blackBold.copyWith(fontSize: 18);

  //s20
  static final blackS20Regular = blackRegular.copyWith(fontSize: 20);
  static final blackS20Italic = blackItalic.copyWith(fontSize: 20);
  static final blackS20Bold = blackBold.copyWith(fontSize: 20);

  //s24
  static final blackS24Regular = blackRegular.copyWith(fontSize: 24);
  static final blackS24Italic = blackItalic.copyWith(fontSize: 24);
  static final blackS24Bold = blackBold.copyWith(fontSize: 24);

  ///White
  static final whiteRegular = regularStyle.copyWith(color: Colors.white);
  static final whiteItalic = italicStyle.copyWith(color: Colors.white);
  static final whiteBold = boldStyle.copyWith(color: Colors.white);

  //s12
  static final whiteS12Regular = whiteRegular.copyWith(fontSize: 12);
  static final whiteS12Italic = whiteItalic.copyWith(fontSize: 12);
  static final whiteS12Bold = whiteBold.copyWith(fontSize: 12);

  //s13
  static final whiteS13Regular = whiteRegular.copyWith(fontSize: 13);
  static final whiteS13Italic = whiteItalic.copyWith(fontSize: 13);
  static final whiteS13Bold = whiteBold.copyWith(fontSize: 13);

  //s14
  static final whiteS14Regular = whiteRegular.copyWith(fontSize: 14);
  static final whiteS14Italic = whiteItalic.copyWith(fontSize: 14);
  static final whiteS14Bold = whiteBold.copyWith(fontSize: 14);

  //s16
  static final whiteS15Regular = whiteRegular.copyWith(fontSize: 15);
  static final whiteS15Italic = whiteItalic.copyWith(fontSize: 15);
  static final whiteS15Bold = whiteBold.copyWith(fontSize: 15);

  //s16
  static final whiteS16Regular = whiteRegular.copyWith(fontSize: 16);
  static final whiteS16Italic = whiteItalic.copyWith(fontSize: 16);
  static final whiteS16Bold = whiteBold.copyWith(fontSize: 16);

  //s17
  static final whiteS17Regular = whiteRegular.copyWith(fontSize: 17);
  static final whiteS17Italic = whiteItalic.copyWith(fontSize: 17);
  static final whiteS17Bold = whiteBold.copyWith(fontSize: 17);

  //s18
  static final whiteS18Regular = whiteRegular.copyWith(fontSize: 18);
  static final whiteS18Italic = whiteItalic.copyWith(fontSize: 18);
  static final whiteS18Bold = whiteBold.copyWith(fontSize: 18);

  //s20
  static final whiteS20Regular = whiteRegular.copyWith(fontSize: 20);
  static final whiteS20Italic = whiteItalic.copyWith(fontSize: 20);
  static final whiteS20Bold = whiteBold.copyWith(fontSize: 20);

  //s24
  static final whiteS24Regular = whiteRegular.copyWith(fontSize: 24);
  static final whiteS24Italic = whiteItalic.copyWith(fontSize: 24);
  static final whiteS24Bold = whiteBold.copyWith(fontSize: 24);

  ///Gray
  static final grayRegular = regularStyle.copyWith(color: Colors.grey);
  static final grayItalic = italicStyle.copyWith(color: Colors.grey);
  static final grayBold = boldStyle.copyWith(color: Colors.grey);

  //s12
  static final grayS12Regular = grayRegular.copyWith(fontSize: 12);
  static final grayS12Italic = grayItalic.copyWith(fontSize: 12);
  static final grayS12Bold = grayBold.copyWith(fontSize: 12);

  //s13
  static final grayS13Regular = grayRegular.copyWith(fontSize: 13);
  static final grayS13Italic = grayItalic.copyWith(fontSize: 13);
  static final grayS13Bold = grayBold.copyWith(fontSize: 13);

  //s14
  static final grayS14Regular = grayRegular.copyWith(fontSize: 14);
  static final grayS14Italic = grayItalic.copyWith(fontSize: 14);
  static final grayS14Bold = grayBold.copyWith(fontSize: 14);

  //s16
  static final grayS15Regular = grayRegular.copyWith(fontSize: 15);
  static final grayS15Italic = grayItalic.copyWith(fontSize: 15);
  static final grayS15Bold = grayBold.copyWith(fontSize: 15);

  //s16
  static final grayS16Regular = grayRegular.copyWith(fontSize: 16);
  static final grayS16Italic = grayItalic.copyWith(fontSize: 16);
  static final grayS16Bold = grayBold.copyWith(fontSize: 16);

  //s17
  static final grayS17Regular = grayRegular.copyWith(fontSize: 17);
  static final grayS17Italic = grayItalic.copyWith(fontSize: 17);
  static final grayS17Bold = grayBold.copyWith(fontSize: 17);

  //s18
  static final grayS18Regular = grayRegular.copyWith(fontSize: 18);
  static final grayS18Italic = grayItalic.copyWith(fontSize: 18);
  static final grayS18Bold = grayBold.copyWith(fontSize: 18);

  //s20
  static final grayS20Regular = grayRegular.copyWith(fontSize: 20);
  static final grayS20Italic = grayItalic.copyWith(fontSize: 20);
  static final grayS20Bold = grayBold.copyWith(fontSize: 20);

  //s24
  static final grayS24Regular = grayRegular.copyWith(fontSize: 24);
  static final grayS24Italic = grayItalic.copyWith(fontSize: 24);
  static final grayS24Bold = grayBold.copyWith(fontSize: 24);

  ///Pink
  static final pinkRegular = regularStyle.copyWith(color: Colors.pink[200]);
  static final pinkItalic = italicStyle.copyWith(color: Colors.pink[200]);
  static final pinkBold = boldStyle.copyWith(color: Colors.pink[200]);

  //s11
  static final pinkS11Regular = pinkRegular.copyWith(fontSize: 11);
  static final pinkS11Italic = pinkItalic.copyWith(fontSize: 11);
  static final pinkS11Bold = pinkBold.copyWith(fontSize: 11);
}
