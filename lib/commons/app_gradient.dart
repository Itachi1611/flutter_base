import 'package:flutter/material.dart';

class AppGradient {
  static final linearGradient = LinearGradient(
    begin: Alignment.bottomCenter,
    end: Alignment.topCenter,
    colors: [Colors.pink[500], Colors.pink[200]], //Flutter bug
  );
}
