import 'dart:ui';

class AppColors {
  ///Common
  static const Color main = Color(0xFFDC9BD5);
  static const Color bgWhite = Color(0xFFFFFFFF);

  ///Shadow
  static const Color shadowColor = Color(0x25606060);
}
