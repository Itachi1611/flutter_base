import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_base/generated/l10n.dart';
import 'package:flutter_base/models/enums/flush_bar_type.dart';
import 'package:flutter_base/utils/flush_bar/flushbar.dart';

class FoxFlushBar {
  static void showFlushBar(BuildContext context, {
      FlushBarType type = FlushBarType.NOTIFICATION,
      String title = "",
      Color titleColor = Colors.white,
      double titleSize = 16.0,
      String message = "",
      double messageSize = 14.0,
      Color messageColor = Colors.white,
      int duration = 3,
      FlushbarPosition position = FlushbarPosition.TOP
  }) {
    Color _backgroundColor;
    Icon _icon;
    String _title;
    switch (type) {
      case FlushBarType.NOTIFICATION:
        _backgroundColor = Color(0xFF63a026);
        _icon = Icon(Icons.info, color: Colors.white);
        _title = "Warning";
        break;
      case FlushBarType.SUCCESS:
        _backgroundColor = Color(0xFF63a026);
        _icon = Icon(Icons.check_circle, color: Colors.white);
        _title = "Success";
        break;
      case FlushBarType.ERROR:
        _backgroundColor = Color(0xFFf64438);
        _icon = Icon(Icons.error, color: Colors.white);
        _title = "Error";
        break;
      case FlushBarType.WARNING:
        _backgroundColor = Color(0xFFedbc38);
        _icon = Icon(Icons.warning, color: Colors.white);
        _title = "Alert";
        break;
    }

    if (title != null && title.isNotEmpty) {
      _title = title;
    }

    Flushbar flushBar;
    flushBar = Flushbar(
      padding: EdgeInsets.only(left: 16, top: 10, right: 16, bottom: 16),
      title: _title,
      titleSize: titleSize,
      titleColor: titleColor,
      message: message != ""
        ? message
        : title == ""
          ? S.current.app_name
          : title,
      messageSize: messageSize,
      messageColor: messageColor,
      duration: Duration(seconds: duration),
      flushbarPosition: position,
      backgroundColor: _backgroundColor,
      icon: _icon,
      shouldIconPulse: true,
      flushbarStyle: FlushbarStyle.GROUNDED,
      dismissDirection: FlushbarDismissDirection.VERTICAL,
      isDismissible: true,
      forwardAnimationCurve: Curves.bounceInOut,
      reverseAnimationCurve: Curves.bounceInOut,
      barBlur: 0.3,
    );
    flushBar.show(context);
  }

  static void showMessageFlushBar(
      BuildContext context, String title, String message) {
    Flushbar flushBar;
    flushBar = Flushbar(
      title: title == ""
          ? S.current.app_name
          : title,
      message: message != ""
          ? message
          : title == ""
          ? S.current.app_name
          : title,
      isDismissible: true,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Color(0xFF63a026),
      icon: Icon(Icons.mail_outline, color: Colors.white),
    );
    flushBar.show(context);
  }

  static void showCoordinationFlushBar(
      BuildContext context, String title, String message) {
    Flushbar flushBar;
    flushBar = Flushbar(
      title: title ?? "",
      message: message ?? "",
      isDismissible: true,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Color(0xFF63a026),
      icon: Icon(Icons.mail_outline, color: Colors.white),
    );
    flushBar.show(context);
  }

  static void showPointFlushBar(
      BuildContext context, String title, String message) {
    Flushbar flushBar;
    flushBar = Flushbar(
      title: title ?? "",
      message: message ?? "",
      isDismissible: true,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Color(0xFF63a026),
      icon: Icon(Icons.mail_outline, color: Colors.white),
    );
    flushBar.show(context);
  }
}