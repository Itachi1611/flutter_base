import 'package:intl/intl.dart';

extension DateTimeExtension on DateTime {
  String toStringWith(String format) {
    String formattedDate = DateFormat(format).format(this);
    return formattedDate;
  }

  String onlyDate({String format = 'dd-MM-yyyy'}) {
    if (this == null) {
      return '';
    }
    return this.toStringWith(format);
  }

  String fullDateTime({String format = 'dd-MM-yyyy HH:mm:ss'}) {
    return this.toStringWith(format);
  }

  String activeDateTime(){
    String text = '';
    if(this != null){
      text = '${this.day} ${this.toStringWith('MMM')}, ${this.toStringWith('h:mm a')} ';
    }
    return text;
  }
}

extension StringExtension on String {
  DateTime toDate(String format) {
    try {
      final date = DateFormat(format).parse(this);
      return date;
    } catch (e) {
      return null;
    }
  }

  bool get isInteger {
    if (this == null || this.trim().isEmpty) {
      return false;
    }
    int result = int.tryParse(this);
    return result != null;
  }

  bool get isDouble {
    if (this == null || this.trim().isEmpty) {
      return false;
    }
    double result = double.tryParse(this);
    return result != null;
  }
}

extension LocalTimeFormatter on String {
  String dateTimeLocal(String format) {
    try {
      final date = DateTime.parse(this).toLocal();
      final dateFormat = DateFormat(format).format(date);
      return dateFormat;
    } catch (e) {
      return null;
    }
  }

  Duration cartRemainingTime(String format) {
    try {
      final date = DateTime.parse(this).toLocal();
      final currentTime = DateTime.now();
      if (currentTime.isAfter(date)) {
        return Duration();
      } else {
        return date.difference(currentTime);
      }
    } catch (e) {
      return Duration();
    }
  }
}
class DateTimeFormatter {
  static String timeFormatDisplay = "hh:mm a";
  static String dateTimeFormatDisplay = "MM.dd.yyyy";

  /// Format date from server và to server;
  static String dateTimeFormat = "yyyy-MM-dd";
  static String timeFormatToServer = "HH:mm";
  static String fullDateTimeFormat = "yyyy-MM-dd'T'hh:mm:ss.SSSZ";
  static String planDateTimeFormat = "yyyy-MM-dd hh:mm:ss";
  static String fullDateTime = "dd-MM-yyyy HH:mm:ss";
}
