import 'package:intl/intl.dart';

class Extension {}

extension ConcurrencyDoubleFormatter on double {
  String get roundDisplay {
    try {
      final _formatter = NumberFormat("#,###.##");
      return "${_formatter.format(this)}";
    } catch (_) {
      return this.toString();
    }
  }
}

extension ConcurrencyIntFormatter on int {
  String get roundDisplay {
    try {
      final _formatter = NumberFormat("#,###.##");
      return "${_formatter.format(this)}";
    } catch (_) {
      return this.toString();
    }
  }
}


