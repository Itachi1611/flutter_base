import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_base/generated/l10n.dart';

class ErrorUtils {
  static String errorToMessage(dynamic e) {
    try {
      if (e is DioError) {
        if (e.type == DioErrorType.CONNECT_TIMEOUT ||
            e.type == DioErrorType.RECEIVE_TIMEOUT ||
            e.type == DioErrorType.SEND_TIMEOUT) {
          return S.current.connection_error;
        }
        if (e.response?.data is Map<String, dynamic>) {
          if (e.response.data['message'] != null) {
            return e.response.data['message'];
          }
        }
        if (e.type == DioErrorType.DEFAULT && e.error is SocketException) {
          return S.current.lost_connect_to_server;
        }

        int statusCode = e?.response?.statusCode;
        if (statusCode != null && statusCode > 500) {
          return S.current.system_error;
        }
      }
    } catch (e) {
      return S.current.system_error;
    }
    return S.current.system_error;
  }
}
