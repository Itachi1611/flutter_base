part of 'app_cubit.dart';

class AppState extends Equatable {
  final TokenEntity token;
  final bool hasConnection;

  AppState({
    this.token,
    this.hasConnection = false,
  });

  AppState copyWith({
    TokenEntity token,
    bool hasConnection,
  }) {
    return AppState(
      token: token ?? this.token,
      hasConnection: hasConnection ?? this.hasConnection,
    );
  }

  @override
  List<Object> get props => [
    token,
    hasConnection,
  ];
}
