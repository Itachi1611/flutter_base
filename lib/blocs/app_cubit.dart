import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_base/configs/global_data.dart';
import 'package:flutter_base/models/entities/token_entity.dart';

part 'app_state.dart';

class AppCubit extends Cubit<AppState> {
  AppCubit() : super(AppState());

  StreamController connectionChangeController = new StreamController.broadcast();

  void onChangeConnectionStatus(ConnectivityResult result) async {
    if (result == ConnectivityResult.wifi || result == ConnectivityResult.mobile) {
      bool previousConnection = state.hasConnection;

      try {
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          emit(state.copyWith(hasConnection: true));
        } else {
          emit(state.copyWith(hasConnection: false));
        }
      } on SocketException catch(_) {
        emit(state.copyWith(hasConnection: false));
      }

      //The connection status changed send out an update to all listeners
      if (previousConnection != state.hasConnection) {
        connectionChangeController.add(state.hasConnection);
      }
    }
  }

  void signOut() async {
    GlobalData.instance.token = null;
    //TODO - Handle logout event
  }

  @override
  Future<void> close() {
    connectionChangeController.close();
    return super.close();
  }
}
