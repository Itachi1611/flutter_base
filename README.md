# flutter_base

# intl_utils
flutter pub run intl_utils:generate

# build_runner
flutter pub run build_runner build 
flutter pub run build_runner build --delete-conflicting-outputs

# Model generate
flutter pub run build_runner build watch --enable-experiment=non-nullable --delete-conflicting-outputs